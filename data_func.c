#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#include "data_func.h"
#include "ip_func.h"




Start_data *load_start_data (void){                 //wczytujemy dane z wejscia
    print("load_start_data");
    Start_data *sdata = malloc(sizeof(Start_data));
    if (!sdata) {
        error("malloc");
    }
    scanf("%d", &sdata->number);
    for(int i=0; i<sdata->number; i++){
        scanf("%16s netmask /%d distance %d",
              &sdata->ip[i][0], &sdata->netmask[i], &sdata->distance[i]);
    }
    //start_print(sdata);
    return sdata;
}

Core_data *start_to_core (Start_data *sdata){           //tworzynu tablice tras
    print("start_to_core");
    Core_data *cdata = malloc(sizeof(Core_data));
    if (!cdata) {
        error("malloc");
    }
    cdata->number = sdata->number;
    char ip[20];
    for(int i=0; i<sdata->number; i++){
        ip_to_network(sdata->ip[i], sdata->netmask[i], ip);
        strncpy(cdata->ip[i], ip, 16);
        cdata->netmask[i] = sdata->netmask[i];
        cdata->status[i] = 0;
    }
    //core_print(cdata);
    return cdata;
}

Tran_data *core_to_tran (Core_data *cdata, char *my_ip){             //tworzymy dane do wysylki
    print("core_to_tran");
    Tran_data *tdata = malloc(sizeof(Tran_data));
    if (!tdata) {
        error("malloc");
    }
    int number = 0;
    strncpy(tdata->my_ip, my_ip, 16);
    for(int i=0; i<cdata->number; i++){
        if(cdata->status[i] == 1 || cdata->status[i] == 2){
            strncpy(tdata->ip[number], cdata->ip[i], 16);
            tdata->netmask[number] = cdata->netmask[i];
            tdata->distance[number] = cdata->distance[i];
            number++;
        }
    }
    tdata->number = number;
    //tran_print(tdata);
    return tdata;
}

void receive_to_core(Start_data *sdata, Core_data *cdata, Tran_data *tdata){
    print("receive_to_core");
    //tran_print(tdata);

    int max_dst = 40;
    int max_ti = 15;

    for(int i=0; i<cdata->number; i++){
        if(cdata->distance[i] > max_dst){
            cdata->status[i] = 0;
        }
    }
    for(int i=0; i<cdata->number; i++){
        if(cdata->ti[i] < time(NULL)-max_ti){
            cdata->status[i] = 0;
            cdata->distance[i] = max_dst*2;
        }
    }

    for(int i=0; i<sdata->number; i++){
        if(strcmp(sdata->ip[i], tdata->my_ip) == 0){
            print("Od siebie");
            return;
        }
    }

    //liczymy dystans do tego od ktorego dostalismy info
    int distance = 666;
    for(int i=0; i<sdata->number; i++){
        if(ip_in_network(sdata->ip[i], sdata->netmask[i], tdata->my_ip)){
            distance = sdata->distance[i];
            i = sdata->number;
            //printf("dystans: %d\t", distance);
            //printf("conected: %s\n", sdata->ip[i]);
        }
    }
    //podajemy bezposredni dostep
    for(int i=0; i<cdata->number; i++){
        if(ip_in_network(cdata->ip[i], cdata->netmask[i], tdata->my_ip)){
            cdata->distance[i] = distance;
            cdata->status[i] = 1;
            cdata->ti[i] = time(NULL);
            //printf("dystans: %d\t", distance);
            //printf("dirlectly to: %s\n", cdata->ip[i]);
        }
    }


    //poprawiamy istniejace dystanse
    for(int i=0; i<cdata->number; i++){
        for(int j=0; j<tdata->number;j++){
            if(cdata->netmask[i] == tdata->netmask[j]){
                if(ip_in_network(cdata->ip[i], cdata->netmask[i], tdata->ip[j])){
                    if(cdata->distance[i] >= tdata->distance[j] + distance){
                        cdata->ti[i] = time(NULL);
                        cdata->status[i] = 2;
                        cdata->distance[i] = tdata->distance[j] + distance;
                        memcpy(cdata->via[i], tdata->my_ip, sizeof(tdata->my_ip));

                    }
                }
            }
        }
    }



    //dodajemy nowe sieci
    bool exis;
    for(int t=0; t<tdata->number; t++){
        exis = false;
        for(int c=0; c<cdata->number; c++){
            if(ip_in_network(cdata->ip[c], cdata->netmask[c], tdata->ip[t])){
                exis = true;
                c = cdata->number;
            }
        }
        if(exis == false){
            memcpy(cdata->ip[cdata->number], tdata->ip[t], sizeof(tdata->my_ip));
            cdata->netmask[cdata->number] = tdata->netmask[t];
            cdata->status[cdata->number] = 2;
            cdata->distance[cdata->number] = tdata->distance[t] + distance;
            memcpy(cdata->via[cdata->number], tdata->my_ip, sizeof(tdata->my_ip));
            cdata->ti[cdata->number] = time(NULL);

            cdata->number++;
        }
    }

    for(int i=0; i<cdata->number; i++){
        if(cdata->distance[i] > max_dst){
            cdata->status[i] = 0;
        }
    }
    for(int i=0; i<cdata->number; i++){
        if(cdata->ti[i] < time(NULL)-max_ti){
            cdata->status[i] = 0;
            cdata->distance[i] = max_dst*2;
        }
    }

}


