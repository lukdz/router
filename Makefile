CC=gcc
CFLAGS=-std=gnu99 -Wall -W
EXECUTABLE = RIP

all: program

program: main.o code_func.o core_data.o com_func.o data_func.o ip_func.o start_data.o tran_data.o
	$(CC) main.o code_func.o core_data.o com_func.o data_func.o ip_func.o start_data.o tran_data.o -o $(EXECUTABLE)
main.o: main.c
	$(CC) -c $(CFLAGS) main.c
code_func.o: code_func.c
	$(CC) -c $(CFLAGS) code_func.c
core_data.o: core_data.c
	$(CC) -c $(CFLAGS) core_data.c
com_func.o: com_func.c
	$(CC) -c $(CFLAGS) com_func.c
data_func.o: data_func.c
	$(CC) -c $(CFLAGS) data_func.c
ip_func.o: ip_func.c
	$(CC) -c $(CFLAGS) ip_func.c
start_data.o: start_data.c
	$(CC) -c $(CFLAGS) start_data.c
tran_data.o: tran_data.c
	$(CC) -c $(CFLAGS) tran_data.c
distclean:
	rm *.o $(EXECUTABLE)
clean:
	rm *.o

