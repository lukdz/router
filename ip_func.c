#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int ip_part(int ip, int mask){
    if(mask >= 8){
        return ip;
    }
    if(mask == 7)
        return ip & (128+64+32+16+8+4+2);
    if(mask == 6)
        return ip & (128+64+32+16+8+4);
    if(mask == 5)
        return ip & (128+64+32+16+8);
    if(mask == 4)
        return ip & (128+64+32+16);
    if(mask == 3)
        return ip & (128+64+32);
    if(mask == 2)
        return ip & (128+64);
    if(mask == 1)
        return ip & (128);
    return 0;
}

int ip_full(int ip, int mask){
    if(mask >= 8){
        return ip;
    }
    if(mask == 7)
        return ip | (1);
    if(mask == 6)
        return ip | (2+1);
    if(mask == 5)
        return ip | (4+2+1);
    if(mask == 4)
        return ip | (8+4+2+1);
    if(mask == 3)
        return ip | (16+8+4+2+1);
    if(mask == 2)
        return ip | (32+16+8+4+2+1);
    if(mask == 1)
        return ip | (64+32+16+8+4+2+1);
    return (128+64+32+16+8+4+2+1);
}

void ip_to_broadcast(char *ip_com, int netmask, char *ip_br){
    int ip1, ip2, ip3, ip4;
    sscanf(ip_com, "%d.%d.%d.%d", &ip1, &ip2, &ip3, &ip4);
    ip1=ip_full(ip1, netmask);
    ip2=ip_full(ip2, netmask-8);
    ip3=ip_full(ip3, netmask-16);
    ip4=ip_full(ip4, netmask-24);
    sprintf(ip_br, "%d.%d.%d.%d", ip1, ip2, ip3, ip4);
}

void ip_to_network(char *ip_com, int netmask, char *ip_net){
    int ip1, ip2, ip3, ip4;
    sscanf(ip_com, "%d.%d.%d.%d", &ip1, &ip2, &ip3, &ip4);
    ip1=ip_part(ip1, netmask);
    ip2=ip_part(ip2, netmask-8);
    ip3=ip_part(ip3, netmask-16);
    ip4=ip_part(ip4, netmask-24);
    sprintf(ip_net, "%d.%d.%d.%d", ip1, ip2, ip3, ip4);
}

bool ip_in_network(char *ip_net, int netmask, char *ip_com){
    char ip[2][20];
    ip_to_network(ip_net, netmask, ip[0]);
    ip_to_network(ip_com, netmask, ip[1]);
    if(strcmp(ip[0], ip[1])==0)
        return true;
    else
        return false;
}
