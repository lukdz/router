#include <stdio.h>

#include "core_data.h"

#include "code_func.h"


void core_print(Core_data *data){
    print("core_print\n");
    printf("%d\n", data->number);
    for(int i=0; i<data->number; i++){
        printf("%s/%d ", data->ip[i], data->netmask[i]);
        //printf(" %ld ", data->ti[i]);
        if(data->status[i] == 0)
            printf("unreachable\n");
        if(data->status[i] == 1)
            printf("distance %d connected directly\n", data->distance[i]);
        if(data->status[i] == 2)
            printf("distance %d via %s\n", data->distance[i], data->via[i]);
    }
}


