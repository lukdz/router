#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <strings.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "code_func.h"
#include "tran_data.h"

#define BUFLEN 512
#define NPACK 10
#define PORT 9930


void send_data(int s, char *ip_dst, Tran_data *data){
    print("send_data");
    print(ip_dst);
    struct sockaddr_in si_other;
    int slen=sizeof(si_other);

    bzero(&si_other, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    if (inet_aton(ip_dst, &si_other.sin_addr)==0) {
        print(ip_dst);
        error("inet_aton() failed");
    }

    print("Sending packet");
    //sprintf(buf, "This is packet %d\n", i);
    sendto(s, data, sizeof(Tran_data), 0, (struct sockaddr*)&si_other, (socklen_t)slen);
    print("Sending complite");
}


Tran_data *recive_data(int s){
    //print("recive_data");
    Tran_data *tdata = malloc(sizeof(Tran_data));
    if (!tdata) {
        error("malloc");
    }

    //print("recvfrom()");
    if(recvfrom(s, tdata, sizeof(Tran_data), MSG_DONTWAIT, 0, 0) == -1){
        //print("recived null");
        return 0;
    }
    //print("Received packet from:");
    //print(tdata->my_ip);
    //print("recived tdata");
    return tdata;
}
