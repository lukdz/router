void ip_to_broadcast(char *ip_com, int netmask, char *ip_br);

void ip_to_network(char *ip_com, int netmask, char *ip_net);

bool ip_in_network(char *ip_net, int netmask, char *ip_com);
