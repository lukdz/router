#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//true - ON, false - OFF
bool debug = false;


void print(char *data){
    if(debug){
        printf("%s\n", data);
    }
}

void error(char *data){
    printf("%s\n", data);
    exit(1);
}

