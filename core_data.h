#include <time.h>

#ifndef core_data
#define core_data

typedef struct{
    int number;
    char ip[4][16];
    int netmask[4];
    int status[4];  //0-unreacheble, 1-directly, 2 - viaip
    int distance[4];
    char via[4][20];
    time_t ti[4];
}Core_data;

void core_print(Core_data *data);

#endif

