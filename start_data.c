#include <stdio.h>

#include "start_data.h"

#include "code_func.h"

void start_print(Start_data *data){
    print("start_print\n");
    printf("%d\n", data->number);
    for(int i=0; i<data->number; i++){
        printf("%s netmask /%d distance %d\n", data->ip[i], data->netmask[i], data->distance[i]);
    }
}



