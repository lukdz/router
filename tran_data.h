#ifndef tran_data
#define tran_data

//wszystkie adresy ip sa przechowywane jako napisy w systemie dziesietnym np. 172.16.1.14
typedef struct{
    int number;        //ilosc osiagalnych sieci
    const int max_number;
    char my_ip[16];    //adres ip komputera nadajacego
    char ip[8][16];     //adres ip osiagalnej sieci
    int netmask[8];     //maska osiagalnej sieci
    int distance[8];    //dystans do osiagalnej sieci
}Tran_data;

void tran_print(Tran_data *data);

//Jak latwo zauwazyc w strukturze mozna przekazywac informacje o maksymalnie 8 dostepnych sieciach. Jest to ustalone maksimum dla programu.

//W przypadku potrzeby przekazywania informacji o wiekszej ilosci sieci program moze wysylac wiecej roznych od siebie komunikatow o dostepnych sieciach. Piszac program nalezy zatem pamietac aby umial sobie radzic z sytuacja, kiedy dostaje informacje o wiecej niz 8 sieciach od jednego nadawcy (oczywiscie nie w jednej wiadomosci).

//za nieosiagalny distance przyjmuje sie 60

#endif



