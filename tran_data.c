#include <stdio.h>

#include "tran_data.h"

void tran_print(Tran_data *data){
    printf("tran_print\n");
    printf("%d\n", data->number);
    for(int i=0; i<data->number; i++){
        printf("%s netmask /%d ", data->ip[i], data->netmask[i]);
    }
}
