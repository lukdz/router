#ifndef data_func
#define data_func

#include "start_data.h"
#include "core_data.h"
#include "tran_data.h"
#include "code_func.h"

Start_data *load_start_data (void);

Core_data *start_to_core (Start_data *sdata);

Tran_data *core_to_tran (Core_data *cdata, char *my_ip);

void receive_to_core(Start_data *sdata, Core_data *cdata, Tran_data *tdata);

#endif
