#ifndef start_data
#define start_data

typedef struct{
    int number;
    char ip[3][16];
    int netmask[3];
    int distance[3];
}Start_data;

void start_print(Start_data *data);

#endif

