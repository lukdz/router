#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


#include "data_func.h"
#include "start_data.h"
#include "core_data.h"
#include "tran_data.h"
#include "com_func.h"
#include "ip_func.h"

int main(){
    print("START");
    Start_data *sdata = load_start_data();
    Core_data *cdata = start_to_core(sdata);

    int s = socket(AF_INET, SOCK_DGRAM, 0);
    int broadcastEnable=1;
    setsockopt(s, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));

	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) {
		fprintf(stderr, "socket error: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	struct sockaddr_in server_address;
	bzero (&server_address, sizeof(server_address));
	server_address.sin_family      = AF_INET;
	server_address.sin_port        = htons(9930);
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind (sockfd, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
		fprintf(stderr, "bind error: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}


    while(true){   ///////////////////DEBUG
        for(int i=0; i<sdata->number; i++){
            Tran_data *tdata1 = core_to_tran(cdata, sdata->ip[i]);
            //tran_print(tdata1);
            char ip_dst[16];
            ip_to_broadcast(sdata->ip[i], sdata->netmask[i], ip_dst);
            send_data(s, ip_dst, tdata1);
            if(errno == 101){
                if(cdata->status[i] == 1){
                    cdata->status[i] = 0;
                }
                errno = 0;
            }
            else{
                cdata->distance[i] = sdata->distance[i];
                cdata->status[i] = 1;
                cdata->ti[i] = time(NULL);
            }
        }
        for(int i=0; i < (sdata->number)*4; i++){
            Tran_data *tdata2 = recive_data(sockfd);
            if(tdata2 != 0){
                receive_to_core(sdata, cdata, tdata2);
            }
        }
        core_print(cdata);
        sleep(10);   /////////////////////////DEBUG
    }


    print("END\n");

    return 0;
}
